package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author USER
 */
public class Lab2 {

    static char[][] table = {{'-', '-', '-'},
    {'-', '-', '-'},
    {'-', '-', '-'}};
static char currentPlayer ='X';
static int row ;
static int col ;
static void printWelcome(){
        System.out.println("Welcome to OX Game");
    } 
    static void printTable(){
        for (int i=0;i<3;i++) {
            for (int j=0;j<3;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println();
        }

    }
    static void printTurn(){
        System.out.println(currentPlayer+" Turn");
    }
    static void inputrow(){
        Scanner sc = new Scanner (System.in);
        System.out.print("Please input the row (1-3): ");
        row = sc.nextInt();
        
    }
    static void inputcol(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input the col (1-3): ");
        col = sc.nextInt();
        
    }
    public static void main(String[] args) {
        printWelcome();
        printTable();
        printTurn();
        inputrow();
        inputcol();
    }
}
